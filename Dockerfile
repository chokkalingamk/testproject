# we are extending everything from tomcat:8.0 image ...
FROM tomcat:8.0
MAINTAINER chokkalingamdevops@gmail.com
# COPY path-to-your-application-war path-to-webapps-in-docker-tomcat
COPY *.war /usr/local/tomcat/webapps/
WORKDIR /usr/local/tomcat/webapps/
#VOLUME /home/chokk/docker_test /usr/local/tomcat/conf
EXPOSE 8080
CMD ["catalina.sh", "run"]